import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:todo_mobx/app/modules/home/components/item/item_widget.dart';
import 'package:todo_mobx/app/modules/home/home_controller.dart';
import 'package:todo_mobx/app/modules/home/home_module.dart';
import 'package:todo_mobx/app/shared/services/models/todo_model.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key key, this.title = "Tarefas"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final controller = HomeModule.to.bloc<HomeController>();
  _showDialogue({TodoModel model}) {
    model = model ?? TodoModel();
    String titleCache = model.title;
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (_) {
          return AlertDialog(
            title: Text(model.id != null ? "Editar" : "Novo"),
            content: TextFormField(
              initialValue: model.title,
              maxLines: 5,
              onChanged: (v) {
                model.title = v;
              },
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    model.title = titleCache;
                    Navigator.pop(context);
                  },
                  child: Text("Cancelar")),
              FlatButton(
                  onPressed: () {
                    if (model.id != null) {
                      controller.update(model);
                    } else {
                      controller.add(model);
                      Navigator.pop(context);
                    }
                  },
                  child: Text("Salvar")),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          Observer(
              builder: (_) => Padding(
                    padding: const EdgeInsets.all(8.0),
                    child:
                        Center(child: Text("Total : ${controller.itemsTotal}")),
                  )),
        ],
        title: Text(widget.title),
      ),
      body: Observer(builder: (_) {
        return ListView.builder(
            itemCount: controller.list.length,
            itemBuilder: (_, index) {
              TodoModel model = controller.list[index];
              return ItemWidget(
                model: model,
                onPressed: () {
                  _showDialogue(model: model);
                },
              );
            });
      }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _showDialogue();
        },
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
    );
  }
}
